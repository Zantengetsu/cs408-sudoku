package sudoProject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class MyAlgo {
    private static final int SIZE = 9;

    public boolean findSolution(SudoBoard board) {
        return findSolution(board, 0);
    }

    private boolean findSolution(SudoBoard board, int cellIndex) {
        if (cellIndex == SIZE*SIZE) return true;
        int row = cellIndex /SIZE, column = cellIndex %SIZE;

        if (board.isFilled(row, column))  return findSolution(board, cellIndex+1);

        List<Integer> optionsForCell = board.getOptionsForCell(row, column);
        for (Integer option : optionsForCell) {
            board.setCellValue(row, column, option);
            if (findSolution(board, cellIndex+1)) return true;
        }
        board.clearCell(row, column);
        return false;
    }

    @SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("C:/Users/Antony/workspace/CS408/SudokuTests/doku-easy.txt"));
        String line;
        while ((line = reader.readLine()) != null) {
            SudoBoard board = new SudoBoard();
            board.readBoard(line);
            System.out.println("Solving: \n" + board.asString());
            MyAlgo solver = new MyAlgo();
            solver.findSolution(board);
            System.out.println("Solved: \n" + board.asString());
        }
    }
}